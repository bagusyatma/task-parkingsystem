import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class App {
    static String db_url = "jdbc:mysql://localhost:3306/bootcamp_parking?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static Connection connection;
    static Statement statement;
    static ResultSet resultSet;
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
//        Parking parking = new Parking();
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(db_url, "root", "");
            statement = connection.createStatement();

            while (!connection.isClosed()){
                showMenu();
            }

            statement.close();
            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    static void showMenu() throws SQLException {
        System.out.println("\n----- Parkir Bro! -----");
        System.out.println("1. mau masuk ?");
        System.out.println("2. apa mau keluar ?");
        System.out.println();
        System.out.println("[1] untuk masuk, [2] untuk keluar, dan [0] untuk menutup program");
        System.out.print("\njadinya? > ");
        String pilihan = input.nextLine();

        switch (pilihan){
            case "0" :
                System.exit(0);
            case "1" :
                in();
                break;
            case "2" :
                close();
                break;
            default:
                System.err.println("\nPILIHANNYA CUMAN 1, 2, 0 BROO!!!");
        }

//        statement.close();
//        connection.close();
    }

    static void in() throws SQLException {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter myParse = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String theTime = now.format(myParse);
        String idParkir = getIdParkir();

        String query = "INSERT INTO masuk(id, jam_masuk) VALUES ('%s', '%s')";
        query = String.format(query, idParkir, theTime);
        statement.execute(query);

        String response = "{\"status_code\" : 200, \"status\" : \"success\", \"data\" : { \"id_parkir\" : \""+ idParkir +"\", \"jam_masuk\" : \""+ theTime +"\" }}";

        System.out.println();
        System.out.println(response);
    }

    static void close(){
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter myParse = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String theTime = now.format(myParse);

        try {
            System.out.print("\ntadi nomor parkirnya berapa bro? > ");
            String inputId = input.nextLine();
            String query = "SELECT * FROM masuk WHERE id = '"+ inputId +"'";
            resultSet = statement.executeQuery(query);
            long harga = 0;

            while (resultSet.next()){
                String idParkir = resultSet.getString("id");
                String timeIn = resultSet.getString("jam_masuk");

                LocalDateTime In = LocalDateTime.parse(timeIn, myParse);
                long hoursBetween = ChronoUnit.HOURS.between(In, now);

//                harga
                int motor = resultSet.getInt("motor");
                int mobil = resultSet.getInt("mobil");

//                harga selanjutnya
                int motorNext = resultSet.getInt("motor_next");
                int mobilNext = resultSet.getInt("mobil_next");

                System.out.print("\nmotor apa mobil nih? > ");
                String tipeKendaraan = input.nextLine().trim();

                if (tipeKendaraan.equalsIgnoreCase("Mobil")){
                    harga = (hoursBetween - 1) * mobilNext + mobil;
                }else if (tipeKendaraan.equalsIgnoreCase("Motor")){
                    harga = (hoursBetween - 1) * motorNext + motor;
                }

                String price = "Rp " + harga;

                System.out.print("\nplat nomornya berapa bro? > ");
                String platNomor = input.nextLine().trim();

                String queryInsert = "INSERT INTO keluar VALUES ('%s', '%s', '%s', '%s', '%s')";
                queryInsert = String.format(queryInsert, idParkir, tipeKendaraan, platNomor, theTime, price);

                statement.execute(queryInsert);

                String response = "{\"status_code\" : 200, \"status\" : \"success\", \"data\" : { \"jam_masuk\" : \""+ timeIn +"\", \"jam_keluar\" : \""+ theTime +"\", \"plat\" : \""+ platNomor +"\", \"id_parkir\" : \""+ idParkir +"\", \"biaya\" : \""+ price +"\"}}";

                System.out.println();
                System.out.println(response);
            }

        }catch (SQLException throwables) {
//            throwables.printStackTrace();
        }

    }

    static String getIdParkir(){
        LocalDateTime now = LocalDateTime.now();

        DateTimeFormatter myParse = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String theTime = now.format(myParse);

        DateTimeFormatter parseYear = DateTimeFormatter.ofPattern("yy");
        String theYear = now.format(parseYear);

        DateTimeFormatter parseMonth = DateTimeFormatter.ofPattern("MM");
        String theMonth = now.format(parseMonth);

        DateTimeFormatter parseDate = DateTimeFormatter.ofPattern("dd");
        String theDate = now.format(parseDate);

        int random_int = (int)(Math.random() * (99 - 11 + 1) + 11);

        String idParkir = "P" + theYear + theMonth + theDate + random_int;

        return idParkir;
    }
}
